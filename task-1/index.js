const { response } = require('express');
const express = require('express')

const app= express()
const port = 3000
const items = require('./items/items.routes')

app.use('/items' ,items)
app.listen(port, ()=> {
    console.log(`Example app listening at http://localhost:${port}`)
})