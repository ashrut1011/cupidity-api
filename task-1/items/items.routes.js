
var bodyParser = require('body-parser')
var express = require('express')
var router = express.Router()
var items = require('./items.json')
findAll = (req, res)=> {
    res.send(items)
}

findOne = (req, res)=> {
    for(var i=0; i<items.length;i++){
        if(items[i].itemCode==req.params.id){
            return res.send(items[i])
        }
    } 
    res.send([])
}


deleteItem = (req, res)=> {
    for(var i=0; i<items.length;i++){
        if(items[i].itemCode==req.params.id){
            items.splice(i,1)

            return res.send("Item deleted successfully")
        }
    } 
    res.send("Could not find item")
}

var jsonParser = bodyParser.json()
editItem = (req, res)=> {
    console.log(req.body)
    for(var i=0; i<items.length;i++){
        if(items[i].itemCode==req.params.id){
            items[i]=req.body
            return res.send("Item edited successfully")
        }
    } 
    res.send("Could not find item")
}

addItem = (req, res)=> {
    console.log(req.body)
    items.push(req.body)
    res.send(items)
}


router.get('/',findAll)
router.get('/:id',findOne)
router.post('/',jsonParser, addItem)
router.put('/:id',jsonParser, editItem)
router.delete('/:id',deleteItem)

module.exports = router